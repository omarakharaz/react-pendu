
import React from 'react'
import ProType from 'prop-types'

import './GuessCount.css'


const GuessCount = ({ guesses, failed }) => (
    <>
    <div className="guesses">number of guesses: {guesses}</div>
    <div className="guesses failed">Number of failed attempts: {failed}</div>
    </>
)

GuessCount.prototype = {
    guesses: ProType.number.isRequired,
    failed: ProType.number.isRequired,
}

export default GuessCount