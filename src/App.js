import React from 'react';
import './App.css';
import { Component } from 'react';
import shuffle from 'lodash.shuffle'
import GuessCount from './GuessCount'
import Keyboard from './Keyboard'
import Letter from './Letter'
import Canvas from './Canvas'

class App extends Component {
  VISIBLE = new Set([' ', '\''])
  MAX_BAD_ATTEMPTS = 10

  state = {
    words: this.shuffleWord(),
    lettresPlay: new Set([]),
    numberFailed: 0,
  }

  componentDidMount() {
    document.addEventListener('keypress', this.onKeyPress)
  }

  componentWillUnmount() {
    document.removeEventListener('keypress', this.onKeyPress)
  }

  // Arrow fx for binding
  onKeyPress = target => {
    if (target.key.match(/[A-Za-z]/)) {
        this.onSelectLetter(target.key.toUpperCase())
    }
  }

  hasWin() {
    const {words, lettresPlay} = this.state

    for (var i=0; i<words.length; i++) {
      if (!this.VISIBLE.has(words[i]) && !lettresPlay.has(words[i].toUpperCase())) {
        return false
      }
    }
    return true
  }

  shuffleWord() {
    const result = []
    const choicesOfWords = [
      'Un mots',
      'Utiliser',
      'Ceci est une phare',
      'Consignes d\'interface',
      'L\'affichage comprend deux parties',
      'Bonjour tout le monde',
      'Openclassrooms',
      'Le meilleur langage reste le cplusplus',
      'Partir en vacances',
    ]
    const word = shuffle(choicesOfWords)[0]
    for (var i=0; i<word.length; i++) {
      result.push(word.charAt(i))
    }
    return result
  }

  // Arrow fx for binding
  onSelectLetter = letter => {
    const {words, lettresPlay, numberFailed} = this.state
    // Check if letter is un word
    // Hidden the letter
    var numFailed = numberFailed
    if (!lettresPlay.has(letter)) {
      if (!words.includes(letter.toLowerCase()) && !words.includes(letter.toUpperCase())) {
        numFailed++
      }
      lettresPlay.add(letter)
      this.setState({ lettresPlay, numberFailed: numFailed })
    }

    var c = document.getElementById('theCanvas')
    var ctx = c.getContext('2d')
    ctx.beginPath()
    if (numFailed === 1) {
      // poutre vertical
      ctx.moveTo(50, 200)
      ctx.lineTo(50, 5)
      ctx.stroke()
    }
    if (numFailed === 2) {
      // poutre horizontal
      ctx.moveTo(50, 5)
      ctx.lineTo(95, 5)
      ctx.stroke()
    }
    if (numFailed === 3) {
      // equerre vertical
      ctx.moveTo(50, 20)
      ctx.lineTo(80, 5)
      ctx.stroke()
    }
    if (numFailed === 4) {
      // corde vertical
      ctx.moveTo(95, 5)
      ctx.lineTo(95, 40)
      ctx.stroke()
    }
    if (numFailed === 5) {
      // tete
      ctx.arc(95,60,20,0,2*Math.PI)
      ctx.stroke()
    }
    if (numFailed === 6) {
      // corps
      ctx.moveTo(95, 80)
      ctx.lineTo(95, 140)
      ctx.stroke()
    }
    if (numFailed === 7) {
      // bras gauche
      ctx.moveTo(95, 90)
      ctx.lineTo(80, 100)
      ctx.stroke()
    }
    if (numFailed === 8) {
      // bras droite
      ctx.moveTo(95, 90)
      ctx.lineTo(110, 100)
      ctx.stroke()
    }
    if (numFailed === 9) {
      // jambe gauche
      ctx.moveTo(95, 140)
      ctx.lineTo(80, 170)
      ctx.stroke()
    }
    if (numFailed === 10) {
      // jambe droite
      ctx.moveTo(95, 140)
      ctx.lineTo(110, 170)
      ctx.stroke()
    }
    ctx.closePath()
  }

  getStatusOfLetter(letter) {
    const {words, lettresPlay} = this.state
    if (this.VISIBLE.has(letter) ||
      (lettresPlay.has(letter.toUpperCase()) && (words.includes(letter.toLowerCase()) || words.includes(letter.toUpperCase())))) {
        return 'visible'
    }
    else {
      return 'hidden'
    }
  }
  reset() {
    var c = document.getElementById('theCanvas')
    var ctx = c.getContext('2d')
    ctx.clearRect(0, 0, c.width, c.height)
    this.setState({
      words: this.shuffleWord(),
      lettresPlay: new Set([]),
      numberFailed: 0,
    })
  }

  render() {
    const {words, lettresPlay, numberFailed } = this.state
    const won = this.hasWin()
    const lose = !won && numberFailed >= this.MAX_BAD_ATTEMPTS
    return (

      <div className="pendu">
        <button onClick={() => this.reset()}>Reset!</button>
        <GuessCount guesses={lettresPlay.size} failed={numberFailed}/>
        <Canvas failed={numberFailed}/>
        {!lose ?
        <>
          {words.map((letter, index) =>(
            <Letter
              letter={letter}
              feedback={this.getStatusOfLetter(letter)}
              key={index}
            />
          ))}
          {won ? <p>Win</p>: <Keyboard plays={lettresPlay} onClick={this.onSelectLetter}/>}
          </>
          : <>
          <div className='lose'> Game over!</div>
          <div className='solution'>The solution is: {words}</div>
          </>}
      </div>
    )
  }
}

export default App;
