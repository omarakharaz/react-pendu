import PropTypes from 'prop-types'
import React from 'react'

import './Keyboard.css'

const HIDDEN_SYMBOL = '_' // add in constant js

const ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
'M', 'N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

const Keyboard = ({plays, onClick}) => (
    <div className='footer'>
    <table className="keyboard">
        <tbody>
        <tr>
            {ALPHABET.map((letter, index) => (
                <td key={index} className="letter" onClick={() => !plays.has(letter)? onClick(letter): undefined}>
                    <button>{plays.has(letter)? HIDDEN_SYMBOL: letter}</button>
                </td>
            ))}
            </tr>
        </tbody>
    </table>
    </div>
)

Keyboard.prototype = {
    plays: PropTypes.arrayOf(
        PropTypes.shape({
            letter: PropTypes.string
        })
    ),
    onClick: PropTypes.func.isRequired,
}

export default Keyboard