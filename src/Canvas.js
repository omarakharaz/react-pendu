import React from 'react'
import PropType from 'prop-types'
import './Canvas.css'


const Canvas = ({failed}) => (
    <div className='canvas'>
        <canvas id='theCanvas' >{failed}</canvas>
    </div>
)

Canvas.prototype = {
    failed: PropType.number.isRequired,
}

export default Canvas