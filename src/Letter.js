import React from 'react'
import PropTypes from 'prop-types'

import './Letter.css'

const HIDDEN_SYMBOL = '_'

const Letter = ({letter, feedback}) => (
        <span className='symbol'>
            {feedback === 'hidden' ? HIDDEN_SYMBOL: letter}
        </span>
)

Letter.prototype = {
    letter: PropTypes.string.isRequired,
    feedback: PropTypes.oneOf([
        'hidden',
        'visible',
    ]).isRequired,
}

export default Letter